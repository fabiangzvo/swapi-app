import axios from 'axios'

export const rejectResponse = (error) => {
  const { message, response: { status = 500, request } = {} } = error

  const messageError = { status, message, request }

  if (!status && !message)
    return { status: 500, error: { message: 'Request error' } }

  console.error('Intercept: ', messageError)

  return messageError
}

export const getResponse = (response) => {
  const { data } = response

  return data
}

const { REACT_APP_SWAPI_API_URL } = process.env

const swapiAxios = axios.create({
  baseURL: `${REACT_APP_SWAPI_API_URL}`,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
})

swapiAxios.interceptors.response.use(getResponse, rejectResponse)

export default swapiAxios
