import axiosConfig from './config'

export const getCharacters = (params) => axiosConfig.get('/people', { params })

export const getPlanet = (planetId) => axiosConfig.get(`/planets/${planetId}`)

export const getCharacterDetail = (characterId) =>
  axiosConfig.get(`/people/${characterId}`)

export const getVehicles = (vehicleId) =>
  axiosConfig.get(`/vehicles/${vehicleId}`)
