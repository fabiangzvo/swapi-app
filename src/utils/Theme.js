export default {
  dark: '#333333',
  light: '#828282',
  error: '#EC5757',
  primary: '#121212',
  white: '#FFFFFF',
}
