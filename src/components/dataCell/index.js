import React from 'react'

import NoticeCell from 'components/noticeCell'
import Typography from 'components/typography'

import { Container } from './style'

function DataCell(props) {
  const { label, value } = props

  return (
    <NoticeCell borderBottom>
      <Container>
        {label && (
          <Typography type='header' color='light'>
            {label}
          </Typography>
        )}
        {value && <Typography type='header'>{value}</Typography>}
      </Container>
    </NoticeCell>
  )
}

export default DataCell
