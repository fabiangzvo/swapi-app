import React from 'react'
import PropTypes from 'prop-types'
import { ArrowLeftOutlined } from '@ant-design/icons'
import { useNavigate } from 'react-router-dom'

import Typography from 'components/typography'

import { Container, Bar, Icon } from './style'

function SectionHeader(props) {
  const { children, title, goBack, isDesktop } = props
  const navigate = useNavigate()
  return (
    <Container data-testid='navbar'>
      {goBack && (
        <Icon onClick={() => navigate(-1)}>
          <ArrowLeftOutlined />
        </Icon>
      )}
      <Bar isdesktop={isDesktop}>
        <Typography color='white' type='header'>
          {title}
        </Typography>
      </Bar>
      {children}
    </Container>
  )
}

SectionHeader.defaultProps = {
  goBack: false,
}

SectionHeader.propTypes = {
  children: PropTypes.any,
  title: PropTypes.string,
  goBack: PropTypes.bool,
}

export default SectionHeader
