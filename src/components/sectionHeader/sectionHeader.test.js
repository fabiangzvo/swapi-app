import React from 'react'
import { render, screen, cleanup } from '@testing-library/react'

import NavBar from './index'

const TITLE = 'Testing Navbar Component'

describe('Typography component test', () => {
  afterEach(cleanup)

  test('Should work correctly', () => {
    render(
      <NavBar title={TITLE}>
        <span>Test</span>
      </NavBar>
    )

    const linkElement = screen.getByTestId('navbar')

    expect(linkElement).toBeInTheDocument()
    expect(linkElement.querySelector('span').textContent).toEqual('Test')
    expect(
      linkElement.querySelector('div > div[data-testid="typography"]').innerHTML
    ).toEqual(TITLE)
  })
})
