import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;

  & > * {
    flex-grow: 1;
  }
`

export const Content = styled.div`
  padding: 32px 16px 8px 16px;
  height: 59px;
  background-color: ${({ theme }) => theme.primary};
`

export const Bar = styled.div`
  height: 59px;
  width: 100%;
  background-color: ${({ theme }) => theme.primary};
  color: white;
  display: flex;
  justify-content: ${({ isdesktop }) => (isdesktop ? 'flex-start' : 'center')};
  align-items: center;
  padding-left: ${({ isdesktop }) => (isdesktop ? '33px' : '0')};
`

export const Icon = styled.div`
  position: absolute;
  display: flex;
  height: 59px;
  align-items: center;
  color: ${({ theme }) => theme.white};
  padding-left: 16px;

  & > span {
    font-size: 24px;
  }
`
