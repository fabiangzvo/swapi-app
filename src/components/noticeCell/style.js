import styled from 'styled-components'

export const Container = styled.div`
  height: 52px;
  display: flex;
  align-items: center;
  border-bottom: ${({ theme, border }) =>
    border && `1px solid rgba(0, 0, 0, 0.1)`};

  & > *:first-child {
    padding: 16px 16px;
    width: 100%;
    text-align: center;
  }
`
