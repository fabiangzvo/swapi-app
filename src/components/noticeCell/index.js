import React from 'react'
import PropTypes from 'prop-types'

import { Container } from './style'

function NoticeCell(props) {
  const { children, borderBottom } = props

  return <Container border={borderBottom}>{children}</Container>
}

NoticeCell.propTypes = {
  children: PropTypes.node,
  borderBottom: PropTypes.bool,
}

export default NoticeCell
