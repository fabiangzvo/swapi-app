import React from 'react'
import { render, screen } from '@testing-library/react'

import LoadingCell from './index'

describe('LoadingCell component test', () => {
  test('renders LoadingCell component', () => {
    render(<LoadingCell />)

    const linkElement = screen.getByText(/Loading/i)

    expect(linkElement).toBeInTheDocument()
  })

  test('check spin visibility', async () => {
    render(<LoadingCell />)

    const linkElement = await screen.findAllByTestId('spinner')

    expect(linkElement[0]).toBeInTheDocument()
    expect(linkElement[0]).toBeVisible()
    expect(linkElement[0].querySelectorAll('div').length).toEqual(12)
  })
})
