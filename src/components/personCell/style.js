import styled from 'styled-components'
import { List } from 'antd'

export const LisItem = styled(List.Item)`
  cursor: pointer;
`
export const IconContainer = styled.div`
  padding-left: 16px;
  padding-right: 16px;
`
