import React, { useEffect, useState, useCallback } from 'react'
import { List } from 'antd'
import { RightOutlined } from '@ant-design/icons'
import { useNavigate } from 'react-router-dom'

import { getPlanet } from 'api'
import Typography from 'components/typography'

import { LisItem, IconContainer } from './style'

const { Meta } = List.Item

function PersonCell(props) {
  const { name, homeworld, url, onClick } = props

  const [planet, setPlanet] = useState({})
  const navigate = useNavigate()

  useEffect(() => {
    async function getPlanetInfo() {
      const response = await getPlanet(homeworld.split('/').at(-2))

      setPlanet(response)
    }

    getPlanetInfo()
  }, [homeworld])

  const handleClick = useCallback(() => {
    const characterId = url.split('/').at(-2)

    if (!onClick(characterId)) navigate(`/character/${characterId}`)
  }, [onClick])

  return (
    <LisItem onClick={handleClick}>
      <Meta
        title={name}
        description={
          <Typography color='light' type='paragraph'>
            Human from ${planet.name}
          </Typography>
        }
      />
      <IconContainer>
        <RightOutlined />
      </IconContainer>
    </LisItem>
  )
}

export default PersonCell
