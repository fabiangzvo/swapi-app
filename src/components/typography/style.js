import styled from 'styled-components'

export const Container = styled.div`
  font-family: 'SF Pro Display', sans-serif;
  color: ${({ theme, color }) => theme[color] || theme.dark};
  font-size: ${({ type = 'paragraph' }) => (type !== 'paragraph' ? 17 : 14)}px;
  font-weight: ${({ type = 'paragraph' }) =>
    type !== 'paragraph' ? 700 : 400};
`
