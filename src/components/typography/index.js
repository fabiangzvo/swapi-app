import React from 'react'
import PropTypes from 'prop-types'

import { Container } from './style'

function Typography(props) {
  const { children, type, color } = props
  return (
    <Container type={type} color={color} data-testid='typography'>
      {children}
    </Container>
  )
}

Typography.defaultProps = {
  type: 'header',
  color: 'dark',
}

Typography.propTypes = {
  type: PropTypes.oneOf(['header', 'paragraph']),
  color: PropTypes.oneOf(['error', 'dark', 'light', 'white']),
  children: PropTypes.node,
}

export default Typography
