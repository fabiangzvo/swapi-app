import React from 'react'
import { render, screen, cleanup } from '@testing-library/react'

import Typography from './index'
import theme from 'utils/Theme'

const TYPOGRAPHY_TEST = 'Testing Typography Component'

describe('Typography component test', () => {
  afterEach(cleanup)

  test('renders LoadingCell component', () => {
    render(<Typography>{TYPOGRAPHY_TEST}</Typography>)

    const linkElement = screen.getByText(TYPOGRAPHY_TEST)

    expect(linkElement).toBeInTheDocument()
  })

  test('header text should render correctly', () => {
    render(<Typography type='header'>{TYPOGRAPHY_TEST}</Typography>)

    const linkElement = screen.getByText(TYPOGRAPHY_TEST)

    expect(linkElement).toHaveStyle({ 'font-weight': 700 })
    expect(linkElement).toHaveStyle({ 'font-size': '17px' })
    expect(linkElement).toHaveStyle({
      'font-family': "'SF Pro Display',sans-serif",
    })
  })

  test('header text error should render correctly', () => {
    render(<Typography color='error'>{TYPOGRAPHY_TEST}</Typography>)

    const linkElement = screen.getByText(TYPOGRAPHY_TEST)

    expect(linkElement).toHaveStyle({ color: theme.secondary })
    expect(linkElement).toHaveStyle({ 'font-weight': 700 })
    expect(linkElement).toHaveStyle({ 'font-size': '17px' })
    expect(linkElement).toHaveStyle({
      'font-family': "'SF Pro Display',sans-serif",
    })
  })

  test('paragraph text should render correctly', async () => {
    render(<Typography type='paragraph'>{TYPOGRAPHY_TEST}</Typography>)

    const linkElement = await screen.getByText(TYPOGRAPHY_TEST)

    expect(linkElement.getAttribute('color')).toBe('dark')
    expect(linkElement).toHaveStyle({ 'font-size': '14px' })
    expect(linkElement).toHaveStyle({
      'font-family': "'SF Pro Display',sans-serif",
    })
  })

  test.only('Should work with default values if a wrong type prop is provided', async () => {
    const logSpy = jest.spyOn(console, 'error')
    render(<Typography type='green'>{TYPOGRAPHY_TEST}</Typography>)

    const linkElement = await screen.getByText(TYPOGRAPHY_TEST)

    expect(logSpy.mock.calls[0][2]).toEqual(
      'Invalid prop `type` of value `green` supplied to `Typography`, expected one of ["header","paragraph"].'
    )
    expect(linkElement).toHaveStyle({ 'font-size': '17px' })
    expect(linkElement).toHaveStyle({ 'font-weight': 700 })
    expect(linkElement).toHaveStyle({
      'font-family': "'SF Pro Display',sans-serif",
    })
  })
})
