import React, { useState, useEffect, useCallback } from 'react'
import useInfiniteScroll from 'react-infinite-scroll-hook'
import { Grid } from 'antd'

import NavBar from 'components/sectionHeader'
import Loader from 'components/loadingCell'
import NoticeCell from 'components/noticeCell'
import Typography from 'components/typography'
import PersonCell from 'components/personCell'
import { getCharacters } from 'api'

import { StyledList, Content, ContentList, ContentDetail } from './style'
import CharacterDetail from '../characterDetail/characterDetail'

const { useBreakpoint } = Grid

export function reducer(state, action) {
  return { ...state, [action.type]: action.value }
}

function CharacterList() {
  const [data, setData] = useState({})
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState(false)
  const [hasMore, setHasMore] = useState(false)
  const [characterId, setCharacterId] = useState(null)

  const breakpoints = useBreakpoint()

  const handleLoadMore = useCallback(async () => {
    if (!data || (data && !data.pagination)) return

    setLoading(true)

    const {
      results = [],
      count,
      next,
    } = await getCharacters({ page: data.pagination })
    setLoading(false)

    if (!results.length) return setError(true)
    const pagination = next ? next.split('=')[1] : null

    setData({ data: [...data.data, ...results], pagination, count })

    setHasMore(!!pagination)
  }, [data])

  useEffect(() => {
    async function getInitialData() {
      setLoading(true)

      const { results = [], count, next } = await getCharacters({ page: 1 })

      setLoading(false)

      if (!results.length) return setError(true)

      const pagination = next ? next.split('=')[1] : null

      setData({ data: results, pagination, count })
      setHasMore(!!pagination)
    }

    getInitialData().catch(() => {
      setLoading(false)
      setError(true)
    })
  }, [])

  const handleClick = useCallback(
    (id) => {
      if (!breakpoints.lg) return false

      setCharacterId(id)

      return true
    },
    [breakpoints]
  )

  const [ref] = useInfiniteScroll({
    loading,
    hasNextPage: hasMore,
    onLoadMore: handleLoadMore,
    rootMargin: '0px 0px 0px 300px',
  })

  return (
    <NavBar title='People of Star Wars' isDesktop={breakpoints.lg}>
      <Content>
        <ContentList isdesktop={breakpoints.lg}>
          {data?.data && (
            <StyledList
              dataSource={data?.data}
              renderItem={(item, i) => (
                <PersonCell key={i} onClick={handleClick} {...item} />
              )}
            />
          )}
          <div ref={ref}>{loading && <Loader />}</div>
          {error && (
            <NoticeCell>
              <Typography color='error'>Failed to Load Data</Typography>
            </NoticeCell>
          )}
        </ContentList>
        <ContentDetail isdesktop={breakpoints.lg}>
          {characterId && breakpoints.lg && (
            <CharacterDetail characterId={characterId} />
          )}
        </ContentDetail>
      </Content>
    </NavBar>
  )
}

export default CharacterList
