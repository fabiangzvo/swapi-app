import styled from 'styled-components'
import { List } from 'antd'

export const StyledList = styled(List)`
  width: 100%;
  padding: 16px 0px 16px 16px;
`

export const Content = styled.div`
  height: 100%;
  display: flex;
`

export const ContentList = styled.div`
  width: ${({ isdesktop }) => (isdesktop ? '350px' : '100%')};
  border-right: 1px solid rgba(0, 0, 0, 0.15);
  height: 100%;
  overflow: auto;
`
export const ContentDetail = styled.div`
  height: 100%;
  width: ${({ isdesktop }) => (isdesktop ? '100%' : 'auto')};
  display: flex;
  justify-content: center;

  & > div {
    width: 80%;
  }
`
