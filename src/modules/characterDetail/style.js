import styled from 'styled-components'

export const Header = styled.div`
  height: 60px;
  display: flex;
  align-items: center;
  padding: 32px 16px 8px 16px;
`
