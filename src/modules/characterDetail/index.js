import React, { useState } from 'react'
import { useParams } from 'react-router-dom'

import SectionHeader from 'components/sectionHeader'

import CharacterDetailComponent from './characterDetail'

function CharacterDetail(props) {
  const { characterId } = useParams()

  const [title, setTitle] = useState('')

  return (
    <SectionHeader title={title} goBack>
      <CharacterDetailComponent setTitle={setTitle} characterId={characterId} />
    </SectionHeader>
  )
}

export default CharacterDetail
