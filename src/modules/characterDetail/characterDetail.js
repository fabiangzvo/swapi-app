import React, { useEffect, useState, useMemo } from 'react'

import Typography from 'components/typography'
import DataCell from 'components/dataCell'
import { getCharacterDetail, getVehicles } from 'api'

import { Header } from './style'

const generalInformation = [
  { label: 'Eye Color', property: 'eye_color' },
  { label: 'Hair Color', property: 'hair_color' },
  { label: 'Skin Color', property: 'skin_color' },
  { label: 'Birth Year', property: 'birth_year' },
]

function CharacterDetail(props) {
  const { characterId, setTitle } = props
  const [data, setData] = useState(null)
  const [vehicles, setVehicles] = useState(null)

  useEffect(() => {
    async function getInitialData() {
      const response = await getCharacterDetail(characterId)

      if (!response || response?.detail) return

      setData(response)
      setTitle && setTitle(response.name)

      Promise.all(
        response.vehicles.map((url) => getVehicles(url.split('/').at(-2)))
      ).then((vehicles) => {
        if (!vehicles || vehicles?.detail) return

        setVehicles(vehicles)
      })
    }

    getInitialData()
  }, [setTitle, characterId])

  const getGeneralInformation = useMemo(() => {
    if (!data) return null

    return generalInformation.map((item, i) => (
      <DataCell key={i} {...item} value={data[item.property]} />
    ))
  }, [data])

  if (!data) return null

  return (
    <div>
      <Header>
        <Typography type='header'>General information</Typography>
      </Header>
      {getGeneralInformation}
      <Header>
        <Typography type='header'>Vehicles</Typography>
      </Header>
      {vehicles &&
        vehicles.map((item, i) => <DataCell key={i} label={item.name} />)}
    </div>
  )
}

export default CharacterDetail
