import React from 'react'
import { Routes, Route, BrowserRouter } from 'react-router-dom'

import CharacterList from 'modules/characterList'
import CharacterDetail from 'modules/characterDetail'

export function Router() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' exact element={<CharacterList />} />
        <Route
          path='character/:characterId'
          exact
          element={<CharacterDetail />}
        />
        <Route path='*' element={() => <div>PEPE</div>} />
      </Routes>
    </BrowserRouter>
  )
}

export default Router
